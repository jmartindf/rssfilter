import feedparser
import PyRSS2Gen as RSS2
import re
import time
from datetime import datetime

def downloadFeeds():
  feed = "http://www.macworld.com/index.rss"
  links = {}
  #urlFix = re.compile("\?mod=.*")

  feed = feedparser.parse(feed)
  for item in feed["entries"]:
    url = item["link"]
    #url = re.sub(urlFix,"",url)
    author = item['author']
    if author == 'The Macalope':
        data = {
            "title": item["title"],
            "description": item["description"],
            "url": url,
            "published": item["published"],
            "author": item["author"]
        }
        links[url] = data
	return links

def createFeed():
	data = downloadFeeds()
	items = []
	try:
		for url in data:
			item = RSS2.RSSItem (
				title = data[url]["title"],
				link = data[url]["url"],
				description = data[url]["description"],
				pubDate = data[url]["published"],
				guid = RSS2.Guid(data[url]["url"]),
	      author = data[url]["author"]
				)
			items.append(item)
		rss = RSS2.RSS2(
			title="Just the Macalope",
	    link="http://www.macworld.com/column/macalope",
			description="Just the Macalope's columns, from MacWorld.",
			lastBuildDate=datetime.now(),
			items=items
			)
		rss.write_xml(open("macalope.xml", "w"))
	except TypeError:
		pass

if __name__=="__main__":
	createFeed()	
