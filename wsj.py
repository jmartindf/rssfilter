import feedparser
import PyRSS2Gen as RSS2
import re
import time
from datetime import datetime

def downloadFeeds():
	feeds = [
		"http://online.wsj.com/xml/rss/3_7198.xml", # Today's Most Popular
		"http://online.wsj.com/xml/rss/3_7087.xml", # Politics and Policy
		"http://online.wsj.com/xml/rss/3_7208.xml", # Personal Journal
		"http://online.wsj.com/xml/rss/3_7205.xml", # Page One
		"http://online.wsj.com/xml/rss/3_7207.xml", # Money and Investing
		"http://online.wsj.com/xml/rss/3_7089.xml", # Health
		"http://online.wsj.com/xml/rss/3_7086.xml" # Economy
		]
	links = {}
	urlFix = re.compile("\?mod=.*")

	for feedURL in feeds:
		feed = feedparser.parse(feedURL)
		for item in feed["entries"]:
			url = item["link"]
			url = re.sub(urlFix,"",url)
			published = datetime.fromtimestamp(time.mktime(item["published_parsed"]))
			diff = datetime.today()-published
			if diff.days > 14:
				continue
			data = {
			"title": item["title"],
			"description": item["description"],
			"url": url,
			"published": published
			}
			links[url] = data
	return links

def createFeed():
	data = downloadFeeds()
	items = []
	for url in data:
		item = RSS2.RSSItem (
			title = data[url]["title"],
			link = data[url]["url"],
			description = data[url]["description"],
			pubDate = data[url]["published"],
			guid = RSS2.Guid(data[url]["url"])
			)
		items.append(item)
	rss = RSS2.RSS2(
		title="Joe's Wall Street Journal",
		link="http://www.wsj.com/",
		description="My Preferred WSJ content as created using my RSSFilter script.",
		lastBuildDate=datetime.now(),
		items=items
		)
	rss.write_xml(open("wsj.xml", "w"))

if __name__=="__main__":
	createFeed()	
