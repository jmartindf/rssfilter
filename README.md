RSS Filter
=============

Troll through multiple RSS feeds. Do some light filtering, then merge everything into one feed.

## Steps to build my virtualenv

### Shell Configuration
    export WORKON_HOME=$HOME/.virtualenvs
    export PROJECT_HOME=$HOME/development
    export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python-2.7
    source /usr/local/bin/virtualenvwrapper.sh

### Commands

    mkvirtualenv rssfilter
    pip install -r requirements.txt
