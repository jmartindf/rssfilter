#!/bin/bash
source $HOME/.local_virtualenv
workon rssfilter
cd $HOME/development/rssfilter
python macalope.py
cp macalope.xml /srv/www/playground/public_html/macalope.xml
